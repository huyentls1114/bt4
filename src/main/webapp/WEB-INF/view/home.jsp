<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>
<style type="text/css">
div {
	text-align: center;
	padding: 25px 25px;
}
</style>
</head>
<body>
	<div>
		<h1 class="ui header">Danh sách user</h1>
		<div>
			<table class="ui selectable celled table">
				<thead>
					<tr>
						<th>User Id</th>
						<th>Name</th>
						<th>Adress</th>
						<th>Birthday</th>
						<th>Married</th>
						<th>Job</th>
						<th colspan=2>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${users}" var="user">
						<tr>
							<td><c:out value="${user.userid}" /></td>
							<td><c:out value="${user.name}" /></td>
							<td><c:out value="${user.adress}" /></td>
							<td><fmt:formatDate pattern="yyyy-MMM-dd"
									value="${user.birthday}" /></td>
							<td><c:out value="${user.married}" /></td>
							<td><c:out value="${user.job}" /></td>
							<td><a
								href="editUser?userid=<c:out value="${user.userid}"/>">Update</a></td>
							<td><a
								href="deleteUser?userid=<c:out value="${user.userid}"/>">Delete</a></td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="7">
							<div class="ui left floated small primary labeled icon button">
								<i class="user icon"></i> <a href="newUser" style="color:white">Add User</a>
							</div>
						</th>
					</tr>
				</tfoot>
			</table>
			<div class="ui right floated small primary labeled icon button">
				<i class="user icon"></i> <a href="${pageContext.request.contextPath}/logout" style="color:white" >Log out</a>
			</div>
		</div>
	</div>


</body>
</html>