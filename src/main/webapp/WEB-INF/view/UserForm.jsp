<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>
<style type="text/css">
#main {
	padding: 25px 250px;
	font-size: 16px;
}

#btn {
	margin: auto;
	padding-top: 20px;
	padding-left: 50%;
	font-size: 30px;
}
</style>
<script language="javascript">
	function validateForm() {
		b = $('#birthday');
		c = new Date(b.val());
		today = new Date();
		if (today.getTime() - c.getTime() < 18 * 24 * 3600000 * 365
				|| isNaN(c.getDate()) || isNaN(c.getYear())
				|| isNaN(c.getMonth())) {
			alert("Invalid birthday");
			return false;
		} else {
			alert("Sucessfull");
			return true;
		}

	}
</script>
</head>
<body>
	<div id="main">
		<form class="ui form" method="POST" action='saveUser' name="frmAddUser" id="formUser" onsubmit="return validateForm()">
			<h1 class="ui dividing header">User Information</h1>
			<div class="two fields">
				<div class="field">
					<label>Username</label> <input type="text" name="username"
						id="username" value="<c:out value="${user.username}" />"
						placeholder="Username">
				</div>
				<div class="field">
					<label>Password</label> <input type="text" name="password"
						id="password" value="<c:out value="${user.password}" />"
						placeholder="Password">
				</div>
			</div>

			<div class="field">
				<label>Name</label>
				<div class="fields">
					<input type="text" name="name" id="name"
						value="<c:out value="${user.name}" />" placeholder="Name">
				</div>
			</div>
			<div class="two fields">
				<div class="twelve wide field">
					<label>Address</label> <input type="text" name="adress" id="adress"
						value="<c:out value="${user.adress}" />" placeholder="Adress">
				</div>
				<div class="four wide field">
					<label>Married</label>
					<select size="1" name="married"">
						<option value=true selected="${user.married}">Yes</option>
						<option value=false selected="${user.married}">No</option>
					</select>
				</div>
			</div>

			<div class="eight wide field">
				<label>Birthday</label> 
				<input type="text" name="birthday" id="birthday" value="<fmt:formatDate pattern="dd/MM/yyyy" value="${user.birthday}" />" placeholder="Birthday">
			</div>

			<div class="eight wide field">
				<label>Job</label> 
				<input type="text" name="job" value="<c:out value="${user.job}" />" placeholder="Job">
			</div>

			<div id="btn" class="ui buttons">
				<button class="ui positive button">Submit</button>
			</div>
		</form>
	</div>


</body>
</html>