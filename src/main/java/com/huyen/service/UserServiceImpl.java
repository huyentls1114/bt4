package com.huyen.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.huyen.dao.UserDao;
import com.huyen.user.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	
	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Transactional
	public void addUser(User user) {
		userDao.addUser(user);
	}

	@Transactional
	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}

	@Transactional
	public void deleteUser(Integer userId) {
		userDao.deleteUser(userId);
	}

	public User updateUser(User user) {
		return userDao.updateUser(user);
	}

	public User getUserById(Integer userId) {
		return userDao.getUserById(userId);
	}
	public Boolean checkUsernamePassword(String username, String password){
		return userDao.checkUsernamePassword(username, password);
	}
	
	
	

}
