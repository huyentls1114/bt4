package com.huyen.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.huyen.user.User;

@Repository
public class UserDaoImpl implements UserDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void addUser(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		
	}
	
	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() {
		return sessionFactory.getCurrentSession().createQuery("from User").list();
	}

	public void deleteUser(Integer userId) {
		User user=(User) sessionFactory.getCurrentSession().load(User.class, userId);
		if(user!=null){
			this.sessionFactory.getCurrentSession().delete(user);
		}
	}

	public User updateUser(User user) {
		sessionFactory.getCurrentSession().update(user);
		return user;
	}

	public User getUserById(Integer userId) {
		return (User)sessionFactory.getCurrentSession().get(User.class, userId);
	}
	
	public Boolean checkUsernamePassword(String username, String password){
		User user=(User) sessionFactory.getCurrentSession().get(User.class,username);
		if(user.getPassword()==password)
			return true;
		else
			return false;
		
	}
	

}
