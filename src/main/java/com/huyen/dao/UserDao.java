package com.huyen.dao;

import java.util.List;

import com.huyen.user.User;

public interface UserDao {
	public void addUser(User user);
	public List<User> getAllUsers();
	public void deleteUser(Integer userId);
	public User updateUser(User user);
	public User getUserById(Integer userId);
	public Boolean checkUsernamePassword(String username, String password);
}
