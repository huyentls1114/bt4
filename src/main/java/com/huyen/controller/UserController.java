package com.huyen.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.huyen.service.UserService;
import com.huyen.user.User;

@Controller
public class UserController {
	private static final Logger logger=Logger.getLogger(UserController.class);
	
	public UserController(){
		System.out.println("UserController()");
	}
	
	@Autowired
	private UserService userService;
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	

	
	@RequestMapping(value="/")
	public ModelAndView listUser(ModelAndView model)throws Exception{
		List<User> listUser=userService.getAllUsers();
		model.addObject("users",listUser);
		model.setViewName("home");
		return model;
	}
	@RequestMapping(value="/demo")
	public String demo(Model model){
		return "demo";
	}
	
	@RequestMapping(value="/newUser",method= RequestMethod.GET)
	public ModelAndView newContact(ModelAndView model){
		User user=new User();
		model.addObject(user);
		model.setViewName("UserForm");
		return model;
	}
	
	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public ModelAndView saveUser(@ModelAttribute User user) {
        if (user.getUserid() == 0) { // if employee id is 0 then creating the
            userService.addUser(user);
        } else {
            userService.updateUser(user);
        }
        return new ModelAndView("redirect:/");
    }
 
    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    public ModelAndView deleteUser(HttpServletRequest request) {
        int userId = Integer.parseInt(request.getParameter("userid"));
        userService.deleteUser(userId);
        return new ModelAndView("redirect:/");
    }
 
    @RequestMapping(value = "/editUser", method = RequestMethod.GET)
    public ModelAndView editContact(HttpServletRequest request) {
        int userId = Integer.parseInt(request.getParameter("userid"));
        User user = userService.getUserById(userId);
        ModelAndView model = new ModelAndView("UserForm");
        model.addObject("user", user);
        return model;
    }
}
